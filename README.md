# README #

__NewClubSporman 2017__    

Leonardo Roselli, Francesco Pivato, Stefano Tamburro

NewClubSportman is free software for anyone who manages a gym.


### How do I get set up? ###

_Instruction for login:_ 

* Admin 1) -> Admin: Leonardo, PW: Leo    
* Admin 2) -> Admin: Stefano, PW: Ste    
* Admin 3) -> Admin: Francesco, PW: Pivot   

### Contribution guidelines ###

* Leonardo Roselli: Model   
* Stefano Tamburro: view   
* Franceso Pivato: Controller   

### Who do I talk to? ###

* Leonardo Roselli: [leonardo.roselli@studio.unibo.it](Link URL)   
* Stefano Tamburro: [stefano.tamburro@studio.unibo.it](Link URL)   
* Francesco Pivato: [francesco.pivato@studio.unibo.it](Link URL)